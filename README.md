Kernels with attached config will work **ONLY** on Lenovo ThinkCentre E73.

```
Intel Core i7-4770S Haswell
Intel Xeon E3-1200 v3/4th Gen Core integrated graphics
Intel 8 Series/C220 Series High Definition Audio
```
**Meet your doctor or pharmacist before eating the package's leaflet.**

**Przed spożyciem ulotki dołączonej do opakowania zapoznaj się z lekarzem lub farmaceutą.**

Best to use with:
- kernel parameter: sched.timeslice=2000
- CONFIG_LL_BRANDING=y in your own config
- optional kernel parameters: cpuidle.governor=teo

Set with:
- performance governor
- NUMA disabled
- 1000 HZ tick rate
- bunch of patches below

Sources:
- [Kernel](https://www.kernel.org/)
- [Arch Linux patches](https://git.archlinux.org/linux.git/)
- [Clear Linux + CVE](https://github.com/clearlinux-pkgs/linux)
- [BMQ](https://gitlab.com/alfredchen/bmq)
- [UKSM](https://github.com/dolohow/uksm)
- [bfq-dev](https://github.com/Algodev-github/bfq-mq/)
- [bfq-lucjan-dev](https://github.com/sirlucjan/bfq-mq-lucjan/)
- [sirlucjan patches](https://github.com/sirlucjan/kernel-patches/)
<!-- **Przed użyciem zapoznaj się z treścią ulotki dołączonej do opakowania bądź skonsultuj się z lekarzem lub farmaceutą, gdyż każdy lek niewłaściwie stosowany zagraża Twojemu życiu lub zdrowiu.**
**Zu Risiken und Nebenwirkungen lesen Sie die Packungsbeilage und fragen Sie Ihren Arzt oder Apotheker.**
**A kockázatokról és mellékhatásokról olvassa el a betegtájékoztatót, vagy kérdezze meg kezelőorvosát, gyógyszerészét.**-->
