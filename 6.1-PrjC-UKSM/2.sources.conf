### SOURCES HARDLINKS{{{
### Arch Linux
# _arch_patches="https://git.archlinux.org/linux.git/patch/"
_arch_patches="https://github.com/archlinux/linux/commit"

### Clear Linux
_cl_path="https://github.com/clearlinux-pkgs/linux/raw/master"

### graysky's gcc
_gcc_path="https://raw.githubusercontent.com/graysky2/kernel_compiler_patch/master"
if [ $( awk -F'.' '{print $2}' <<< "$pkgver" ) -lt 17 ]; then
	_gcc_patch="more-uarches-for-kernel-5.15-5.16.patch"
else
	_gcc_patch="more-uarches-for-kernel-5.17+.patch"
fi

### Paolo Valente's bfq-dev
# _paolo_path="https://github.com/Algodev-github/bfq-mq/commit"

### github/pavbaranov
# _pavbaranov_path="https://raw.githubusercontent.com/pavbaranov/kernel-pb/master"

### Alfred Chen ProjectC
_projc_path="https://gitlab.com/alfredchen/projectc/raw/master/${_major}"
_projc_ver="prjc_v${_major}-r1.patch"

## Łukasz Żarnowiecki UKSM
# _uksm_path='https://raw.githubusercontent.com/dolohow/uksm/master'
# _uksm_ver="v5.x/uksm-${_major}.patch"

### gitlab/sirlucjan patches versions
_lucjan_path="https://gitlab.com/sirlucjan/kernel-patches/raw/master/${_major}"

# _uksm_patches_ver="-v3"
_ksm_patches_ver=""
_prjc_patches_ver="-v6"
_prjc_fixes_ver="-v6"

_block_ll_patches_ver=""
# _lucjan_bfq_dev_ver="v14-r2K210223"
_lucjan_bfq_mainver="r2K220619v1"
_lucjan_bfq_secver="-v11"

_btrfs_patches_ver="-v11"
_cpupower_patches_ver=""
_clearlinux_patches_ver="-v5"
_fm_patches_ver="-v23"
_kbuild_patches_ver="-v4"
_lqx_patches_ver="-v2"
_pf_patches_ver=""
_v4l2loopback_patches_ver=""
_xanmod_patches_ver=""
_zen_patches_ver="-v2"
_zstd_ll_patches_ver=""
_bbr2_patches_ver="-v2"
_futex_patches_ver=""
_lru_pf_patches_ver=""
_mm_patches_ver=""

## realtime
# _rtsrc="5.6.4"
# _rtver="rt3"
#}}}

### SOURCES makepkg array {{{
source=(
  ## Arch kernel
  #"$_srcname::git+https://git.archlinux.org/linux.git?signed#tag=$_srctag"

  ## stable kernel sources
  "https://cdn.kernel.org/pub/linux/kernel/v${_basekernline}.x/${_srcname}.tar.xz"
  "https://cdn.kernel.org/pub/linux/kernel/v${_basekernline}.x/${_srcname}.tar.sign"

  ## mainline kernel sources
  #"https://git.kernel.org/torvalds/t/${_srcname}.tar.gz"

  ## RT sources
  # "http://www.kernel.org/pub/linux/kernel/projects/rt/${_major}/patch-${_rtsrc}-${_rtver}.patch.xz"
  # "http://www.kernel.org/pub/linux/kernel/projects/rt/${_major}/patch-${_rtsrc}-${_rtver}.patch.sign"

  ## Arch patches {{{
  "arch1_ZEN-Add-sysctl-and-CONFIG-to-disallow-unprivileged-CLONE_NEWUSER.patch::${_arch_patches}/3cc300f383b8041c97534eca79c035b55c804a30.patch"
  "arch1_docs-Fix-the-docs-build-with-Sphinx-6.0.patch::${_arch_patches}/a3b216b0a5c2628624de605599aa6c16675578de.patch"
  "arch1_Revert-drm-display-dp_mst-Move-all-payload-info-into-the-atomic-state.patch::${_arch_patches}/2dac8857deaeb3d9e22ae48f9c6e85ced2fd8d96.patch"
  "arch1_netfilter-nft_payload-incorrect-arithmetics-when-fetching-VLAN-header-bits.patch::${_arch_patches}/07fa6df737871f5e491ec70f499963aedd679f2a.patch"
  # "${_lucjan_path}/arch-patches/0001-arch-patches.patch"
  #}}}

  ## projC patch {{{
  # "${_projc_path}/${_projc_ver}"
  # "lucjan_prjc-patches${_prjc_patches_ver}.patch::${_lucjan_path}/prjc-patches${_prjc_patches_ver}/0001-PRJC-for-${_major}.patch"
  # "lucjan_prjc-fixes${_prjc_fixes_ver}.patch::${_lucjan_path}/prjc-fixes${_prjc_fixes_ver}/0001-prjc-fixes.patch"
  #}}}

## U/KSM {{{
  ## Łukasz Żarnowiecki UKSM-patches
  # "${_uksm_path}/${_uksm_ver}"
  ## gitlab/sirlucjan
  # "lucjan_ksm-patches${_ksm_patches_ver}.patch::${_lucjan_path}/ksm-patches${_ksm_patches_ver}/0001-ksm-patches.patch"
#}}}

## gitlab/sirlucjan {{{

  ## bfq/bfq-dev patches {{{
  ## block patches - gitlab/sirlucjan
  # "lucjan_block-ll-patches${_block_ll_patches_ver}.patch::${_lucjan_path}/block-ll-patches${_block_ll_patches_ver}/0001-block-patches.patch"

  # "${_major}-bfq-lucjan-ll${_lucjan_bfq_secver}_${_lucjan_bfq_mainver}-all.patch::${_lucjan_path}/bfq-lucjan-ll${_lucjan_bfq_secver}-all/0001-bfq-lucjan.patch"
  # }}}

  ## btrfs patches - gitlab/sirlucjan
  # "lucjan_btrfs-patches${_btrfs_patches_ver}.patch::${_lucjan_path}/btrfs-patches${_btrfs_patches_ver}/0001-btrfs-patches.patch"

  ## różne/fm - gitlab/sirlucjan
  # "lucjan_fm-patches${_fm_patches_ver}.patch::${_lucjan_path}/fixes-miscellaneous${_fm_patches_ver}/0001-fixes-miscellaneous.patch"

  ## postfactum Oleksandr Natalenko patches - gitlab/sirlucjan
  # "lucjan_pf-fixes${_pf_fixes_ver}.patch::${_lucjan_path}/pf-fixes${_pf_fixes_ver}/0001-pf-patches.patch"

  ## v4l2 loopback - gitlab/sirlucjan
  # "lucjan_v4l2loopback-patches${_v4l2loopback_patches_ver}.patch::${_lucjan_path}/v4l2loopback-patches${_v4l2loopback_patches_ver}/0001-media-v4l2-core-add-v4l2loopback.patch"

  ## xanmod autogroup kernel param - gitlab/sirlucjan
  # "lucjan_xanmod-patches${_xanmod_patches_ver}.patch::${_lucjan_path}/xanmod-patches${_xanmod_patches_ver}/0001-xanmod-patches.patch"

  ## zen patches - gitlab/sirlucjan
  # "lucjan_zen-patches${_zen_patches_ver}.patch::${_lucjan_path}/zen-patches${_zen_patches_ver}/0001-zen-patches.patch"

  ## zstd patches - gitlab/sirlucjan
  # "lucjan_zstd-ll-patches${_zstd_ll_patches_ver}.patch::${_lucjan_path}/zstd-ll-patches${_zstd_ll_patches_ver}/0001-zstd-patches.patch"
  # "${_lucjan_path}/zstd-dev-patches/0001-zstd-dev-patches.patch"
#}}}

  'config'        # the main kernel config file
  'linux.preset'  # mkinitcpio ramdisk config file

  ## gcc cpu optimizatons from graysky and ck
  "${_gcc_path}/${_gcc_patch}"

  ## Clear Linux patches {{{
  # "cl-0001.patch::${_cl_path}/0001-x86-sched-Decrease-further-the-priorities-of-SMT-sib.patch"
  # "cl-0002.patch::${_cl_path}/0002-sched-topology-Introduce-sched_group-flags.patch"
  # "cl-0003.patch::${_cl_path}/0003-sched-fair-Optimize-checking-for-group_asym_packing.patch"
  # "cl-0004.patch::${_cl_path}/0004-sched-fair-Provide-update_sg_lb_stats-with-sched-dom.patch"
  # "cl-0005.patch::${_cl_path}/0005-sched-fair-Carve-out-logic-to-mark-a-group-for-asymm.patch"
  # "cl-0006.patch::${_cl_path}/0006-sched-fair-Consider-SMT-in-ASYM_PACKING-load-balance.patch"
  # "cl-0101.patch::${_cl_path}/0101-i8042-decrease-debug-message-level-to-info.patch"
  # "cl-0102.patch::${_cl_path}/0102-increase-the-ext4-default-commit-age.patch"
  # "cl-0103.patch::${_cl_path}/0103-silence-rapl.patch"
  # "cl-0104.patch::${_cl_path}/0104-pci-pme-wakeups.patch"
  # "cl-0105.patch::${_cl_path}/0105-ksm-wakeups.patch"
  # "cl-0106.patch::${_cl_path}/0106-intel_idle-tweak-cpuidle-cstates.patch"
  # "cl-0107.patch::${_cl_path}/0107-bootstats-add-printk-s-to-measure-boot-time-in-more-.patch"
  # "cl-0108.patch::${_cl_path}/0108-smpboot-reuse-timer-calibration.patch"
  # "cl-0109.patch::${_cl_path}/0109-initialize-ata-before-graphics.patch"
  # "cl-0110.patch::${_cl_path}/0110-give-rdrand-some-credit.patch"
  # "cl-0111.patch::${_cl_path}/0111-ipv4-tcp-allow-the-memory-tuning-for-tcp-to-go-a-lit.patch"
  # "cl-0113.patch::${_cl_path}/0112-init-wait-for-partition-and-retry-scan.patch"
  # "cl-0114.patch::${_cl_path}/0113-print-fsync-count-for-bootchart.patch"
  # "cl-0115.patch::${_cl_path}/0114-add-boot-option-to-allow-unsigned-modules.patch"
  # "cl-0116.patch::${_cl_path}/0115-enable-stateless-firmware-loading.patch"
  # "cl-0117.patch::${_cl_path}/0116-migrate-some-systemd-defaults-to-the-kernel-defaults.patch"
  # "cl-0118.patch::${_cl_path}/0117-xattr-allow-setting-user.-attributes-on-symlinks-by-.patch"
  # "cl-0119.patch::${_cl_path}/0118-add-scheduler-turbo3-patch.patch"
  # "cl-0120.patch::${_cl_path}/0119-use-lfence-instead-of-rep-and-nop.patch"
  # "cl-0121.patch::${_cl_path}/0120-do-accept-in-LIFO-order-for-cache-efficiency.patch"
  # "cl-0122.patch::${_cl_path}/0121-locking-rwsem-spin-faster.patch"
  # "cl-0123.patch::${_cl_path}/0122-ata-libahci-ignore-staggered-spin-up.patch"
  # "cl-0124.patch::${_cl_path}/0123-print-CPU-that-faults.patch"
  # "cl-0125.patch::${_cl_path}/0124-x86-microcode-Force-update-a-uCode-even-if-the-rev-i.patch"
  # # "cl-0126.patch::${_cl_path}/0125-x86-microcode-echo-2-reload-to-force-load-ucode.patch"
  # "cl-0127.patch::${_cl_path}/0126-fix-bug-in-ucode-force-reload-revision-check.patch"
  # "cl-0128.patch::${_cl_path}/0127-nvme-workaround.patch"
  # "cl-0129.patch::${_cl_path}/0128-don-t-report-an-error-if-PowerClamp-run-on-other-CPU.patch"
  # "lucjan_clearlinux-patches${_clearlinux_patches_ver}.patch::${_lucjan_path}/clearlinux-patches${_clearlinux_patches_ver}/0001-clearlinux-patches.patch"
  #}}}

  ## Clear Linux CVE {{{
  # "${_cl_path}/CVE-2019-12379.patch"
  #}}}
)

## gitlab/sirlucjan c.d. {{{
## bbrv2 - natalenko/google ipv4 - gitlab/sirlucjan
# if [ -n "$_bbr2_tcp_cong" ]; then
  # source+=("lucjan_bbr2-patches${_bbr2_patches_ver}.patch::${_lucjan_path}/bbr2-patches${_bbr2_patches_ver}/0001-bbr2-${_major}-introduce-BBRv2.patch")
# fi

## futex2 - natalenko - gitlab/sirlucjan
# if [ -n "$_futex" ]; then
  # source+=("${_lucjan_path}/futex-patches-v2/0001-futex-resync-from-gitlab.collabora.com.patch")
  # source+=("lucjan_futex-patches${_futex_patches_ver}.patch::${_lucjan_path}/futex-patches${_futex_patches_ver}/0001-futex-Add-entry-point-for-FUTEX_WAIT_MULTIPLE-opcode.patch")
# fi
## futex - gitlab/sirlucjan
  # source+=("${_lucjan_path}/futex-zen-patches/0001-futex-resync-from-gitlab.collabora.com.patch")

# if [ -n "$_ntfs3" ]; then
  ## ntfs3 patches - gitlab/sirlucjan
  # source+=("${_lucjan_path}/ntfs3-patches-v2/0001-ntfs3-patches.patch")
# fi

# if [ -n "$_lru_enable" ]; then
  ## lru patches - gitlab/sirlucjan
  # source+=("lucjan_lru-pf-patches${_lru_patches_pf_ver}.patch::${_lucjan_path}/lru-pf-patches${_lru_patches_pf_ver}/0001-lru-patches.patch" )
	# "lucjan_prjc-lru-patches-0001.patch::${_lucjan_path}/prjc-lru-patches/0001-sched-alt-Add-MG-LRU-changes-through-ifdef-macro.patch" )
# elif [ -n "$_mm_protect" ]; then
  ## mm patches - gitlab/sirlucjan
  # source+=("lucjan_mm-patches${_mm_patches_ver}.patch::${_lucjan_path}/mm-patches${_mm_patches_ver}/0001-mm-${_major}-protect-mappings-under-memory-pressure.patch")
# fi
#}}}

#}}}

# unused {{{
# if [ -n "$_security" ]; then
#   ## security - gitlab/sirlucjan
#   source+=("${_lucjan_path}/security-patches/0001-security-patches.patch")
# fi

## virtio sound driver
# if [ -n "$_virtio_snd" ]; then
#   source+=("vs1.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=0ae0337f929a970ee8d83e0e95e6b8d05562ce3b"
# 		"vs2.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=de3a9980d8c34b2479173e809afa820473db676a"
# 		"vs3.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=9d45e514da88ff74fc24ffb34e7d6eb92576440b"
# 		"vs4.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=29b96bf50ba958eb5f097cdc3fbd4c1acf9547a2"
# 		"vs5.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=f40a28679e0b7cb3a9cc6627a8dbb40961990f0a"
# 		"vs6.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=da76e9f3e43a7195c69d370ee514cccae6517c76"
# 		"vs7.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=ca61a41f389c80db091db9d4ad5a651e2b4c9f70"
# 		"vs8.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=19325fedf245ca932c58a629d3888a9a393534ab"
# 		"vs9.patch::https://git.kernel.org/pub/scm/linux/kernel/git/tiwai/sound.git/patch/?id=575483e90a3292c2afceb7161732046e411d6fdd")
# fi
#}}}
